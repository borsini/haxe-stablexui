package;

import ru.stablex.ui.UIBuilder;
import ru.stablex.ui.widgets.Scroll;
import ru.stablex.ui.widgets.Text;
import openfl.display.FPS;

class NBody {

public static function compute(){
   var start = haxe.Timer.stamp();

   var n:Int = 5000;

      var bodies : NBodySystem = new NBodySystem();

      trace("{0:f9}",bodies.Energy());
      for (i in 0...n) {
         bodies.Advance(0.01);
      }

      trace("{0:f9}",bodies.Energy());

      var duration = haxe.Timer.stamp() - start;
      trace(' took '+duration + ' seconds');

      var text = UIBuilder.create(
            Text,{
                  text : ' took '+duration + ' seconds',
                  top : 30
              }
           );

      flash.Lib.current.addChild(text);
}

}
class NBodySystem {
   var  bodies : Array<Body>;
            
   public function new() {        
      bodies = [
         Body.Sun(),    
         Body.Jupiter(),
         Body.Saturn(),
         Body.Uranus(),
         Body.Neptune()                                  
      ];
      
      var px : Float = 0.0;
      var py : Float = 0.0;  
      var pz : Float = 0.0;         

      for (body in bodies) {                                         
         px += body.vx * body.mass;
         py += body.vy * body.mass;    
         pz += body.vz * body.mass;          
      }     

      bodies[0].OffsetMomentum(px,py,pz);
   }
                        
   public function Advance(dt:Float) : Void{
      var dx, dy, dz, distance, mag : Float;   
   
      for (i in 0...bodies.length) {
         for (j in i+1...bodies.length) { 
            dx = bodies[i].x - bodies[j].x;
            dy = bodies[i].y - bodies[j].y;
            dz = bodies[i].z - bodies[j].z;
            
            distance = Math.sqrt(dx*dx + dy*dy + dz*dz);             
            mag = dt / (distance * distance * distance);
            
            bodies[i].vx -= dx * bodies[j].mass * mag;
            bodies[i].vy -= dy * bodies[j].mass * mag;
            bodies[i].vz -= dz * bodies[j].mass * mag;
                                
            bodies[j].vx += dx * bodies[i].mass * mag;
            bodies[j].vy += dy * bodies[i].mass * mag;
            bodies[j].vz += dz * bodies[i].mass * mag;
         }
      }     
      
      for(body in bodies) {
         body.x += dt * body.vx;
         body.y += dt * body.vy;
         body.z += dt * body.vz;
      }     
   }        
   
   public function Energy() : Float{      
      var dx, dy, dz, distance : Float;  
      var e : Float = 0.0;         
      
      for (i in 0...bodies.length) {
         e += 0.5 * bodies[i].mass * 
            ( bodies[i].vx * bodies[i].vx 
            + bodies[i].vy * bodies[i].vy 
            + bodies[i].vz * bodies[i].vz );
            
         for (j in i+1...bodies.length) {
            dx = bodies[i].x - bodies[j].x;
            dy = bodies[i].y - bodies[j].y;
            dz = bodies[i].z - bodies[j].z;
                                
            distance = Math.sqrt(dx*dx + dy*dy + dz*dz);
            e -= (bodies[i].mass * bodies[j].mass) / distance;
         }
      }
      return e;
   }                                                                                                        
}


class Body {
   public static inline var PI : Float = 3.141592653589793;   
   public static inline var SOLAR_MASS : Float = 4 * PI * PI;
   public static inline var DAYS_PER_YEAR : Float = 365.24;

   public var x : Float = 0.0;
   public var y : Float = 0.0;
   public var z : Float = 0.0; 
   public var vx : Float = 0.0;
   public var  vy : Float = 0.0;
   public var   vz : Float = 0.0;
   public var    mass : Float = 0.0;
   
   public function new(){}

   public static function Jupiter():Body {
      var p = new Body();
      p.x = 4.84143144246472090e+00;
      p.y = -1.16032004402742839e+00;
      p.z = -1.03622044471123109e-01;
      p.vx = 1.66007664274403694e-03 * DAYS_PER_YEAR;
      p.vy = 7.69901118419740425e-03 * DAYS_PER_YEAR;
      p.vz = -6.90460016972063023e-05 * DAYS_PER_YEAR;
      p.mass = 9.54791938424326609e-04 * SOLAR_MASS;                 
      return p;
   }  
   
   public static function Saturn():Body  {
      var p = new Body();
      p.x = 8.34336671824457987e+00;
      p.y = 4.12479856412430479e+00;
      p.z = -4.03523417114321381e-01;
      p.vx = -2.76742510726862411e-03 * DAYS_PER_YEAR;
      p.vy = 4.99852801234917238e-03 * DAYS_PER_YEAR;
      p.vz = 2.30417297573763929e-05 * DAYS_PER_YEAR;
      p.mass = 2.85885980666130812e-04 * SOLAR_MASS;                 
      return p;
   }  
   
   public static function Uranus():Body  {
      var p = new Body();
      p.x = 1.28943695621391310e+01;
      p.y = -1.51111514016986312e+01;
      p.z = -2.23307578892655734e-01;
      p.vx = 2.96460137564761618e-03 * DAYS_PER_YEAR;
      p.vy = 2.37847173959480950e-03 * DAYS_PER_YEAR;
      p.vz = -2.96589568540237556e-05 * DAYS_PER_YEAR;
      p.mass = 4.36624404335156298e-05 * SOLAR_MASS;                    
      return p;
   }     

   public static function Neptune():Body  {
      var p = new Body();
      p.x = 1.53796971148509165e+01;
      p.y = -2.59193146099879641e+01;
      p.z = 1.79258772950371181e-01;
      p.vx = 2.68067772490389322e-03 * DAYS_PER_YEAR;
      p.vy = 1.62824170038242295e-03 * DAYS_PER_YEAR;
      p.vz = -9.51592254519715870e-05 * DAYS_PER_YEAR;
      p.mass = 5.15138902046611451e-05 * SOLAR_MASS;                 
      return p;
   }
   
   public static function Sun():Body  {
      var p = new Body();
      p.mass = SOLAR_MASS;                
      return p;
   }        
   
   public function OffsetMomentum(px:Float, py:Float, pz:Float):Body  {
      vx = -px / SOLAR_MASS;
      vy = -py / SOLAR_MASS;
      vz = -pz / SOLAR_MASS;     
      return this;   
   }                          
}
