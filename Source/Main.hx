package;

import flash.display.StageAlign;
import flash.display.StageScaleMode;
import flash.Lib;
import ru.stablex.ui.UIBuilder;
import ru.stablex.ui.widgets.Widget;
import flash.events.MouseEvent;
import openfl.display.FPS;
import openfl.events.Event;
import ru.stablex.ui.widgets.Text;
import ru.stablex.ui.widgets.Bmp;
import openfl.Assets;
import ru.stablex.ui.widgets.VBox;
import ru.stablex.ui.widgets.HBox;
import ru.stablex.ui.skins.Paint;



/**
* Simple demo project for StablexUI
*/
class Main extends Widget{


	public  var myImages:Array<String>;
	public var txt:Text;
	public var nbImages:Int = 0;


	public function new () {
		Lib.current.stage.align     = StageAlign.TOP_LEFT;
		Lib.current.stage.scaleMode = StageScaleMode.NO_SCALE;
		UIBuilder.regClass('Main');
		UIBuilder.buildFn('main.xml')().show();

		var fps:FPS = new FPS(0, 0, 0x999999);
		flash.Lib.current.addChild(fps);

		var listBtn : Widget = UIBuilder.getAs('listBtn', Widget);

		listBtn.addEventListener(
			MouseEvent.CLICK,
			function(event:flash.events.MouseEvent){
				var parent : VBox = UIBuilder.getAs('box', VBox);

				var bd = Assets.getBitmapData("assets/1.png");

				for(i in 0...1000){

					var box = UIBuilder.create(
						HBox,
						{
						widthPt : '100'
						}
					);


					var img = UIBuilder.create(
						Bmp,
						{
						bitmapData : bd,
						w : '100',
						h : '100'
						}
					);


					var text = UIBuilder.create(
						Text,{
						text : 'My first widget! ' + Std.string(i)
						}
					);

					if( box.skin == null ) box.skin = new Paint();
					cast(box.skin, Paint).color  = 0xdddddd;
					cast(box.skin, Paint).border = 1;

					box.addChild(img);
					box.addChild(text);

					parent.addChild(box);
				}

			});

		var imagesBtn : Widget = UIBuilder.getAs('imagesBtn', Widget);

			imagesBtn.addEventListener(
				MouseEvent.CLICK,
				function(event:flash.events.MouseEvent){
					this.myImages = new Array<String>();
					myImages.push('assets/1.png');
					myImages.push('assets/2.png');
					myImages.push('assets/3.jpg');
					myImages.push('assets/4.jpg');

					txt = UIBuilder.create(
						Text,{
						top:30,
						left:0,
						format     : {
						color  : 0xFF0000,
						size   : 22
						}
						}
					);

					flash.Lib.current.addChild(txt);

					stage.addEventListener(Event.ENTER_FRAME, onEnterFrame);

				});

		var forceBtn : Widget = UIBuilder.getAs('forceBtn', Widget);

				forceBtn.addEventListener(
					MouseEvent.CLICK,
					function(event:flash.events.MouseEvent){
						trace('NBodies');

						 NBody.compute();
					}
				);

		super();
	}

	public inline function RandInRange(from:Int, to:Int):Int
	{
		return from + Math.floor(((to - from + 1) * Math.random()));
	}


	private function onEnterFrame(event: Event): Void {

		nbImages ++;
		var idx = RandInRange( 0, myImages.length - 1 );

		var img = UIBuilder.create(
			Bmp,
			{
			left:RandInRange( 0, 300 ),
			top:RandInRange( 40, 300 ),
			src : myImages[idx],
			width : '100',
			height : '100',
			}
		);

		trace(idx);

		flash.Lib.current.addChild(img);
		txt.text = Std.string(nbImages);
	}

}


